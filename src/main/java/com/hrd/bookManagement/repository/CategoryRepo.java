package com.hrd.bookManagement.repository;

import com.hrd.bookManagement.model.Books;
import com.hrd.bookManagement.model.Categories;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface CategoryRepo {

    @Insert("INSERT INTO tb_categories (id, title) VALUES (#{id}, #{title})")
    boolean insertCategory(Categories categories);

    @Select("SELECT * FROM tb_categories")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "title", column = "title"),
    })

    List<Categories> getCategories();


    @Delete("DELETE FROM tb_categories WHERE id=#{id}")
    void delete(int id);

    @Update("UPDATE tb_categories SET title=#{title} WHERE id=#{id} ")
    void update(int id, String title);

    @Select("SELECT * FROM tb_categories limit #{recordNum} offset #{page}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "title", column = "title"),
    })

    List<Categories> getByPagination(int page, int recordNum);
}
