package com.hrd.bookManagement.repository;

import com.hrd.bookManagement.model.Books;
import com.hrd.bookManagement.model.Categories;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.awt.print.Book;
import java.util.List;
@Repository
public interface BookRepo {
    @Insert("INSERT INTO tb_books (id, title, author, description, thumbnail, category_id) " +
            "VALUES (#{id}, #{title}, #{author}, #{description}, #{thumbnail}, #{category_id})")
    boolean insertBooks(Books books);

    @Select("SELECT * FROM tb_books")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "title", column = "title"),
            @Result(property = "author", column = "author"),
            @Result(property = "description", column = "description"),
            @Result(property = "thumbnail", column = "thumbnail"),
            @Result(property = "category_id", column = "category_id"),
    })
    List<Books> getBooks();

    @Select("SELECT * FROM tb_books WHERE id=#{id}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "title", column = "title"),
            @Result(property = "author", column = "author"),
            @Result(property = "description", column = "description"),
            @Result(property = "thumbnail", column = "thumbnail"),
            @Result(property = "category_id", column = "category_id"),
    })
    Books findOne(int id);

    @Select("SELECT * FROM tb_books WHERE category_id=#{category_id} AND title LIKE #{title}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "title", column = "title"),
            @Result(property = "author", column = "author"),
            @Result(property = "description", column = "description"),
            @Result(property = "thumbnail", column = "thumbnail"),
            @Result(property = "category_id", column = "category_id"),
    })
    List<Books> findByCateIDorTitle(int category_id, String title);
    @Delete("DELETE FROM tb_books WHERE id=#{id}")
    void delete(int id);

    @Update("UPDATE tb_books SET title=#{title}, author=#{author}, description=#{description}, thumbnail=#{thumbnail} " +
            ",category_id=#{category_id} WHERE id=#{_id}")
    void update(int _id, String title, String author, String description, String thumbnail, int category_id);

    @Select("SELECT * FROM tb_books limit #{recordNum} offset #{page}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "title", column = "title"),
            @Result(property = "author", column = "author"),
            @Result(property = "description", column = "description"),
            @Result(property = "thumbnail", column = "thumbnail"),
            @Result(property = "category_id", column = "category_id"),
    })

    List<Books> getByPagination(int page, int recordNum);
}
