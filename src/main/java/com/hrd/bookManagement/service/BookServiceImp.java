package com.hrd.bookManagement.service;

import com.hrd.bookManagement.model.Books;
import com.hrd.bookManagement.repository.BookRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookServiceImp implements BookService {

    @Autowired
    BookRepo bookRepo;

    @Override
    public List<Books> findAll(){
        return bookRepo.getBooks();
    }

    @Override
    public List<Books> getByPagination(int page, int recordNum) {
        return bookRepo.getByPagination(page-1,recordNum);
    }

    @Override
    public List<Books> findbyTitleAndcateId(int cateId, String title) {
        return bookRepo.findByCateIDorTitle(cateId,'%'+title+'%');
    }

    @Override
    public Books findOne(int id) {
    return bookRepo.findOne(id);
    }


    @Override
    public void delete(int id) {
        bookRepo.delete(id);
    }

    @Override
    public void insert(Books books) {
        bookRepo.insertBooks(books);
    }

    @Override
    public void update(int _id, String title, String author, String description, String thumbnail, int category_id) {
        bookRepo.update(_id,title,author,description,thumbnail,category_id);

    }


}
