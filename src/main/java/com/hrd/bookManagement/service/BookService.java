package com.hrd.bookManagement.service;

import com.hrd.bookManagement.model.Books;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public interface BookService {
    List<Books> findAll();
    List<Books> getByPagination(int page, int recordNum);
    List<Books> findbyTitleAndcateId(int cateId, String title);
    Books findOne(int id);
    void delete(int id);
    void insert(Books books);
    void update(int _id, String title, String author, String description, String thumbnail, int category_id);
}
