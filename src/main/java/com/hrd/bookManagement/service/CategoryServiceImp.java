package com.hrd.bookManagement.service;

import com.hrd.bookManagement.model.Books;
import com.hrd.bookManagement.model.Categories;
import com.hrd.bookManagement.repository.BookRepo;
import com.hrd.bookManagement.repository.CategoryRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImp implements CategoryService {
    @Autowired
    CategoryRepo categoryRepo;

    @Override
    public List<Categories> findAll() {
        return categoryRepo.getCategories();
    }

    @Override
    public List<Categories> getByPagination(int page, int recordNum) {
       return categoryRepo.getByPagination(page-1, recordNum);
    }

    @Override
    public void deleteCate(int id) {
        categoryRepo.delete(id);
    }


    @Override
    public void insert(Categories categories) {
        categoryRepo.insertCategory(categories);
    }

    @Override
    public void update(int id, String title) {
        categoryRepo.update(id,title);
    }

}



