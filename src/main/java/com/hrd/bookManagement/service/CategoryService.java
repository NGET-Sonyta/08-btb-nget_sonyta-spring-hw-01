package com.hrd.bookManagement.service;

import com.hrd.bookManagement.model.Books;
import com.hrd.bookManagement.model.Categories;

import java.util.List;

public interface CategoryService {
    List<Categories> findAll();
    List<Categories> getByPagination(int page, int recordNum);
    void deleteCate(int id);
    void insert(Categories categories);
    void update(int id, String title);
}
