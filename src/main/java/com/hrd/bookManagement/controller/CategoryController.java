package com.hrd.bookManagement.controller;


import com.hrd.bookManagement.model.Books;
import com.hrd.bookManagement.model.Categories;
import com.hrd.bookManagement.service.BookService;
import com.hrd.bookManagement.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/categories")
public class CategoryController {
    CategoryService categoryService;

    @Autowired
    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }


    @GetMapping()
    @PreAuthorize("hasAnyRole('ROLE_USER','ROLE_ADMIN')")

    public ResponseEntity<List<Categories>> findAll(){
        return new ResponseEntity<>(categoryService.findAll(), HttpStatus.OK);
    }

    @GetMapping("/page")
    @PreAuthorize("hasAnyRole('ROLE_USER','ROLE_ADMIN')")

    public ResponseEntity<List<Categories>> getByPagination(@RequestParam int page, @RequestParam int recordNum){
        return new ResponseEntity<>(categoryService.getByPagination(page, recordNum), HttpStatus.OK);
    }

    @PostMapping()
    @PreAuthorize("hasRole('ROLE_ADMIN')")

    public ResponseEntity<String>insert(@RequestBody Categories categories){
        categoryService.insert(categories);
        return new ResponseEntity<>("Added", HttpStatus.CREATED);

    }

    @PutMapping("/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")

    public ResponseEntity<String> update(@RequestParam int id, @RequestBody String title){
        System.out.println(title);
        categoryService.update(id, title);
        return ResponseEntity.ok("Updated");
    }

    @DeleteMapping("{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")

    public ResponseEntity<String>delete(@PathVariable int id){
        categoryService.deleteCate(id);
        return ResponseEntity.ok("Deleted");
    }



}
