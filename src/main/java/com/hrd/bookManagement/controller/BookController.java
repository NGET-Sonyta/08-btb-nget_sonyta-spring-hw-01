package com.hrd.bookManagement.controller;

import com.hrd.bookManagement.model.Books;
import com.hrd.bookManagement.model.ResponseMessage;
import com.hrd.bookManagement.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/books")
public class BookController {
    BookService bookService;

    @Autowired
    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

    @PreAuthorize("hasAnyRole('ROLE_USER','ROLE_ADMIN')")
    @GetMapping()
    public ResponseEntity<List<Books>> findAll() {
        try {
            List<Books> obj = bookService.findAll();
        }catch (Exception e)
        {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(bookService.findAll(), HttpStatus.OK);
    }

    @GetMapping("/pagination")
    @PreAuthorize("hasAnyRole('ROLE_USER','ROLE_ADMIN')")

    public ResponseEntity<List<Books>> getByPagination(@RequestParam int page, @RequestParam int recordNum){
        return new ResponseEntity<>(bookService.getByPagination(page, recordNum), HttpStatus.OK);
    }

    @PostMapping()
    @PreAuthorize("hasRole('ROLE_ADMIN')")

    public ResponseEntity<String>insert(@RequestBody Books books){
        String message = "";

        try {

            bookService.insert(books);
       }catch (Exception e){
           return new ResponseEntity<>(e.toString(), HttpStatus.INTERNAL_SERVER_ERROR);
       }
        return new ResponseEntity<>("Added", HttpStatus.CREATED);
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAnyRole('ROLE_USER','ROLE_ADMIN')")
    public ResponseEntity<Books>findOne(@PathVariable int id){
        return new ResponseEntity<>(bookService.findOne(id),HttpStatus.OK);
    }

    @PutMapping("/{_id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")

    public ResponseEntity<String> update(@PathVariable String _id,@RequestBody Books books ){
        bookService.update(Integer.parseInt(_id),books.getTitle(),books.getAuthor(),books.getDescription(),books.getThumbnail(),books.getCategory_id());
        return ResponseEntity.ok("Updated");
    }
    @GetMapping("/findBycateAndtitle")
    @PreAuthorize("hasAnyRole('ROLE_USER','ROLE_ADMIN')")

    public ResponseEntity<List<Books>> getBycateIdAndTitle(@RequestParam Integer cateId,@RequestParam String title){
        return new ResponseEntity<>(bookService.findbyTitleAndcateId(cateId,title),HttpStatus.OK);
    }
    @DeleteMapping("{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")

    public ResponseEntity<String>deleteBook(@PathVariable int id){
        bookService.delete(id);
        return ResponseEntity.ok("Deleted");
    }

}
