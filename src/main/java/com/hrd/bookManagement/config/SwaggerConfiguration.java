package com.hrd.bookManagement.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfiguration {

    ApiInfo apiInfo(){
        return new ApiInfoBuilder()
                .title("Book Management System API")
                .description("Korea Software HRD Center")
                .license("License of API")
                .licenseUrl("Email")
                .termsOfServiceUrl("https://spring.io/")
                .version("8th Basic Course")
                .contact(new Contact("by Nget Sonyta", "https://spring.io/", "ngetsonyta1999@gmail.com"))
                .build();
    }


    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.hrd.bookManagement.controller"))
                .build().apiInfo(apiInfo());
    }

}
